AppLaravel

### Developer
* Regita Lisgiani

### Built With
* [Laravel](https://laravel.com/)
* [Laravel Jetstream](https://jetstream.laravel.com/)

### Package Use
* [Firebase JWT](https://github.com/firebase/php-jwt)


### Installation

1. First Clone the project 
```sh
git clone https://gitlab.com/regitalisgianidrajat/applaravel8.git
```
2. Whenever you clone a new Laravel project you must now install all of the project dependencies. This is what actually installs Laravel itself, among other necessary packages to get started.When we run composer, it checks the composer.json file which is submitted to the github repo and lists all of the composer (PHP) packages that your repo requires. Because these packages are constantly changing, the source code is generally not submitted to github, but instead we let composer handle these updates. So to install all this source code we run composer with the following command.
```sh
composer install
```
3. Here I'm using Jetstream, so you should install and build your NPM dependencies and migrate your database:
```sh
npm install
npm run dev
php artisan migrate
```
3. Setup the database 
4. Run the seeder by  type (in ter terminal)
 ```sh
php artisan db:seed
```
5. Do php artisan server in the terminal and run the application
6. You can Register the account in the link register
    Register URL  http://127.0.0.1:8000/register
    Login URL http://127.0.0.1:8000/login
7. After Login you can see the JetStream UI Which already provide the logout button in the top right corner
8. In the menu bar you can also clik the user list, which is show list of the user


### API DOCUMENTATION

1. Import the postman Documentation [BridgeNoteIndonesia](https://www.getpostman.com/collections/a07c409ffecd1aff4be2)
2. Setup the postman variables [URL_DEV : http://127.0.0.1:8000/]
3. Get the Credential Token to Access the API (Request Name : Get Token API). Below are the access to get the token credential  
```sh
user:developer001
secret:developer001-bb18fbeef2-1613324558
```
4. Place the token in the barrier token header 
    


### Note
I'm not bulding the Auth for the CMS, all the Auth and page already built by Laravel Jetstream. Laravel Jetstream provides the implementation for your application's login, registration, email verification, two-factor authentication, session management, API via Laravel Sanctum.






