<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AuthAPI\CredentialModel;
use App\Http\Traits\GeneralTrait;

class CredentialController extends Controller
{
    use GeneralTrait;

    public function Create(Request $request)
    {
        $PostRequest = $request->only(
            'user',
            'secret'
        );

        $role = [
            'user' => 'Required|String',
            'secret' => 'Required|String'
        ];
        $ValidateData = $this->ValidateRequest($PostRequest, $role);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $check = CredentialModel::select('*')->where('user','=',$request['user'])->where('secret','=',$request['secret'])->first();
        if (!empty($check)) {
            return $this->GenerateToken($check);
        }else{
            return $this->ResponseJson(406, 'Failed! Credential not found make sure you enter correct user and secret data!',(object)array());
        }
        
    }


}
