<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserPositionModel;
use App\Http\Traits\GeneralTrait;

class UserPositionController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = UserPositionModel::select('*')->with('user')->get();	
		if (!$getData->isEmpty()) {
			return $this->ResponseJson(200,"User Position",$getData);
		}else{
			return $this->ResponseJson(404,"User Position Not Found",array());
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'Required|integer',
            'position' => 'Required|string',
            'status' => 'Required|string|in:active,inactive'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = UserPositionModel::create($request->all());
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"User Position succesfully added",$save);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $getData = UserPositionModel::select('*')->with('user')->where('id',$request->id)->first();	
		if (!empty($getData)) {
			return $this->ResponseJson(200,"User Position Detail",$getData);
		}else{
			return $this->ResponseJson(404,"User Position Not Found",array());
		}
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'Required|integer',
            'user_id' => 'Required|integer',
            'position' => 'Required|string',
            // 'status' => 'Required|string|in:active,inactive'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = UserPositionModel::where('id',$request->id)->update($request->except(['_method','id']));
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"User Position succesfully updated",$save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'Required|integer'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $delete = UserPositionModel::where('id',$request->id)->delete();
        if(!$delete){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"User Position succesfully deleted",$delete);
    }
}
