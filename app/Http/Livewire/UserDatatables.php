<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class UserDatatables extends Component
{
    public $search = '';
    public function render()
    { 
        $users = !empty($this->search) ? User::where('name', 'like', '%'.$this->search.'%')->get() : User::all();

        return view('livewire.user-datatables',compact('users'));
    }
}
