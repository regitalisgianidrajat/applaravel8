<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class CheckCredentialToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'status' => false,
                'message' => 'Token not provided.'
            ], 401);
        }
        try {
            $app = "applaravel8.0";
            $credentials = JWT::decode($token, $app, ['HS256']);
            
        } catch(ExpiredException $e) {
            return response()->json([
                'status' => false,
                'message' => 'Provided token is expired.'
            ], 402);
        } catch(Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Auth - '.$e->getMessage().' - '.$e->getFile().' - L '.$e->getLine()
            ],(method_exists($e, 'getStatusCode')) ? $e->getStatusCode() : 500);
        }
        return $next($request);
    }
}
