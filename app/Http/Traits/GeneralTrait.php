<?php

namespace App\Http\Traits;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Firebase\JWT\JWT;

trait GeneralTrait {

    public function GenerateToken($credential) {
        $app = "applaravel8.0";
        $payload = [
            'iss' => "laravel-jwt", // Issuer of the token
            'sub' => $credential->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60*24,// Expiration time
            'name' => $credential->name,
            'secret' => $credential->secret,
            'user' => $credential->user,

        ];
        $response['TOKEN'] = JWT::encode($payload, $app);
        return $this->ResponseJson(200, 'Success! Token Generated !', $response);
    }

    public function ResponseJson($status,$message,$data = null){
		if($status == 200){
			$response = [
				'status' => true,
				'message' => $message,
				'data' => $data
			];
		}else{
			$response = [
				'status' => false,
				'message' => $message
			];
		}
		return response()->json($response, $status);
	}
    function ValidateRequest($params,$rules){

		$validator = Validator::make($params, $rules);

		if ($validator->fails()) {
			$response = [
				'status' => false,
				'message' => $validator->messages()
			];
			return response()->json($response, Response::HTTP_NOT_ACCEPTABLE);
		}
	}   
}