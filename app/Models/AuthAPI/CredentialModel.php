<?php

namespace App\Models\AuthAPI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CredentialModel extends Model
{
    use HasFactory;
    protected $table   = 'credential';
	public $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
		'name',
		'user',
		'secret'
	];
	protected $hidden = [
		'created_at',
		'updated_at'
    ];

}
