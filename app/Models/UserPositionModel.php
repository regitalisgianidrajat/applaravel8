<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPositionModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table   = 'user_position';
	public $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'status',
        'position',
    ];

    protected $hidden = [
        'deleted_at',
        'updated_at'
    ];

    public function user() {
		return $this->belongsTo('App\Models\User', 'user_id','id');
	}
}
