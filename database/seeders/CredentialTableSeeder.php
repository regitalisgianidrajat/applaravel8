<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AuthAPI\CredentialModel;

class CredentialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credential = new CredentialModel;
        $credential->name = "developer001";
        $credential->user = "developer001";
        $credential->secret =  "developer001-".substr(md5(mt_rand()), 0, 10).'-'.time();
        $credential->save();
    }
}
