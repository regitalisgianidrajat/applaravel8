<div class="w-full min-h-screen p-8">
    <div class="flex justify-center w-full">
        <div class="flex justify-end w-2/3 pb-8">
            <input class="px-2 text-sm border border-gray-300 rounded shadow" wire:model="search" type="text" placeholder="Search User">
        </div>
    </div>
    <div class="flex justify-center w-full">
        <table class="w-2/3 border border-collapse border-gray-800 shadow">
            <thead>
                <tr>
                    <th class="p-2 text-sm text-gray-500 bg-white border border-gray-300">Email</th>
                    <th class="p-2 text-sm text-gray-500 bg-white border border-gray-300">Name</th>
                    <th class="p-2 text-sm text-gray-500 bg-white border border-gray-300">Created At</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key)
                    <tr>
                        <td class="p-2 text-sm bg-white border border-gray-300 text-gray">{{ $key->email }}</td>
                        <td class="p-2 text-sm bg-white border border-gray-300 text-gray">{{ $key->name }}</td>
                        <td class="p-2 text-sm text-center bg-white border border-gray-300 text-gray">{{ $key->created_at->toDateTimeString() }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
