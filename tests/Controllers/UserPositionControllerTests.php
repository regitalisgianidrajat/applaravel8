<?php
    
namespace Tests\Feature;
    
use Illuminate\Http\Response;
use Tests\TestCase;

class UserPositionControllerTests extends TestCase {
    public function testIndexReturnsDataInValidFormat() {
        $this->json('get', 'api/user-position')
        ->assertStatus(Response::HTTP_OK)
        ->assertJsonStructure(
            [
                'data' => [
                    '*' => [
                        'id',
                        'user_id',
                        'status',
                        'position',
                        'created_at',
                        'user' => [
                            'id',
                            'name',
                            'email',
                            'email_verified_at',
                            'created_at',
                            'profile_photo_url'
                        ]
                    ]
                ]
            ]
        );
    }
    public function testUserIsCreatedSuccessfully()
    {
        $payload = [
        'user_id' => 1,
        'position'  => $this->faker->name,
        'status'    => 'active'
    ];
        $this->json('post', 'api/user-position', $payload)
         ->assertStatus(Response::HTTP_OK)
         ->assertJsonStructure(
             [
                 'data',
                 'message',
                 'status'
             ]
         );
        // $this->assertDatabaseHas('users', $payload);
    }
}